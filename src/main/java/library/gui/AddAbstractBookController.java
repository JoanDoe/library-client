package library.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
//import library.dao.impl.AbstractBookDaoImpl;
//import library.dao.impl.AuthorDaoImpl;
import library.TaskTransaction;
import library.model.AbstractBook;
import library.model.Author;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.ZoneId;
import java.util.*;

public class AddAbstractBookController implements Initializable {
	@FXML
	private static Stage addAbstractBookStage;

	@FXML
	private ComboBox authorComboBox;

	@FXML
	private ListView<String> authorsListView;

	@FXML
	private TextField titleTextField;

	@FXML
	private TextField genreTextFiled;

	@FXML
	private DatePicker publicationDatePicker;

	@FXML
	private BorderPane borderPane;

	private HashMap<String, Integer> authorsMap = new HashMap<String, Integer>();

	ObservableList<String> authorsList = FXCollections.observableArrayList();

	public static Stage getAddAbstractBookStage() {
		return addAbstractBookStage;
	}

	public static void setAddAbstractBookStage(Stage addAbstractBookStage) {
		AddAbstractBookController.addAbstractBookStage = addAbstractBookStage;
	}

	@FXML
	public void initialize(URL url, ResourceBundle resourceBundle) {
		authorsListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		setComboBoxItems();
	}

	private void setComboBoxItems(){
		authorsMap.clear();
		authorsList.clear();
		authorsListView.getItems().clear();

		try {
			List<Author> authors = (List<Author>) TaskTransaction.run("Author", "getAll");
			for (Author author : authors) {
				authorsMap.put(author.getLastName(), author.getId());
				authorsList.add(author.getLastName());
			}
			authorsListView.setItems(authorsList);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}


	public void addAuthorButtonClicked() throws SQLException {
		AddAuthorController.getAddAuthorStage().showAndWait();
		setComboBoxItems();
	}

	public void OKButtonClicked() throws IOException, ClassNotFoundException {
		AbstractBook newAbstractBook = new AbstractBook();
		Set<Author> authorSet = new HashSet<Author>();

		int i = authorsListView.getSelectionModel().getSelectedItems().size();
		String title = titleTextField.getText();
		if (!((authorsListView.getSelectionModel().getSelectedItems().size() > 0)) || (titleTextField.getText().equals(""))) {
			createErrorDialog("A book should have an author and a title!");
		} else {
			for (String s : authorsListView.getSelectionModel().getSelectedItems()) {
				//authorSet.add(authorDao.get(authorsMap.get(s)));
                authorSet.add((Author) TaskTransaction.run("Author", "get", (Object) authorsMap.get(s)));
            }

			newAbstractBook.setAuthors(authorSet);

			newAbstractBook.setTitle(titleTextField.getText());

			if (!genreTextFiled.getText().equals("")) {
				newAbstractBook.setGenre(genreTextFiled.getText());
			}
			if (publicationDatePicker.getValue() != null)
				newAbstractBook.setPublicationDate(Date.from(publicationDatePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));

			//abstractBookDao.add(newAbstractBook);
            TaskTransaction.run("AbstractBook", "add",(Object) newAbstractBook);
			getAddAbstractBookStage().hide();
			clearAllAreas();
		}
	}

	private void clearAllAreas() {
		for (Node node : borderPane.getChildren()) {
			if (node instanceof VBox) {
				for (Node childNode : ((VBox) node).getChildren()) {
					if (childNode instanceof Label) {
						continue;
					}
					if (childNode instanceof TextField) {
						((TextField) childNode).clear();
						continue;
					}
					if (childNode instanceof ComboBox) {
						((ComboBox) childNode).getSelectionModel().clearSelection();
						continue;
					}
				}
			}
		}
	}

	public void cancelButtonClicked() {
		getAddAbstractBookStage().hide();
		clearAllAreas();
	}

	private void createErrorDialog(String errorMessage) {
		Alert alert = new Alert(Alert.AlertType.ERROR, errorMessage, ButtonType.OK);
		alert.initModality(Modality.WINDOW_MODAL);
		alert.setTitle("Error!");
		alert.showAndWait();
	}
}
