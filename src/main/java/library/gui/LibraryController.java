package library.gui;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
//import library.controller.BookExporter;
//import library.controller.BookImporter;
//import library.dao.impl.BookDaoImpl;
import library.TaskTransaction;
import library.model.AbstractBook;
import library.model.Book;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

public class LibraryController implements Initializable {

    public static ObservableList<Book> searchAndUpdatedResult = FXCollections.observableArrayList();
    public static boolean isSearchAndUpdatedResultReturned = false;


    @FXML
    private static Stage modalStage;

    @FXML
    private static Stage abstractBookStage;

    @FXML
    private static Stage libraryStage;

    @FXML
    private static Stage addBookStage;

    @FXML
    private static Stage deleteStage;

    @FXML
    private static Stage updateBookStage;

    private static AbstractBook aBookToTransmit;
    @FXML
    public TableView<Book> dataTableView;

    @FXML
    private ChoiceBox DBChoiceBox;
    @FXML
    private Button createButton;
    @FXML
    private Button updateButton;
    @FXML
    private Button deleteButton;
    @FXML
    private Button importButton;
    @FXML
    private Button exportButton;
    @FXML
    private Button searchButton;
    @FXML
    private Button addButton;
    @FXML
    private TableColumn<Book, String> abstractBookColumn;
    @FXML
    private TableColumn<Book, Integer> pagesQuantityColumn;
    @FXML
    private TableColumn<Book, String> isGivenColumn;
    @FXML
    private TableColumn<Book, String> publishingHouseColumn;
    @FXML
    private TableColumn<Book, String> languageColumn;
    @FXML
    private TableColumn<Book, Integer> isbnColumn;

    private ObservableList<Book> booksInLibrary = FXCollections.observableArrayList();

    private FileChooser fileChooser = new FileChooser();

    public static Stage getModalStage() {
        return modalStage;
    }

    public static void setModalStage(Stage modalStage) {
        LibraryController.modalStage = modalStage;
    }

    public static Stage getAbstractBookStage() {
        return abstractBookStage;
    }

    public static void setAbstractBookStage(Stage abstractBookStage) {
        LibraryController.abstractBookStage = abstractBookStage;
    }

    public static Stage getLibraryStage() {
        return libraryStage;
    }

    public static void setLibraryStage(Stage libraryStage) {
        LibraryController.libraryStage = libraryStage;
    }

    public static Stage getAddBookStage() {
        return addBookStage;
    }

    public static void setAddBookStage(Stage addStage) {
        LibraryController.addBookStage = addStage;
    }

    public static Stage getUpdateBookStage() {
        return updateBookStage;
    }

    public static void setUpdateBookStage(Stage updateBookStage) {
        LibraryController.updateBookStage = updateBookStage;
    }

    public static Stage getDeleteStage() {
        return deleteStage;
    }

    public static void setDeleteStage(Stage deleteStage) {
        LibraryController.deleteStage = deleteStage;
    }

    @FXML
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //TaskTransaction.initSocket();
        setCellValues();

        try {
            showAllBooks();
        } catch (ClassNotFoundException e){}
        catch (IOException e){}

        dataTableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 1) {
                    Node node = ((Node) event.getTarget()).getParent();
                    TableRow row;
                    if (node instanceof TableRow) {
                        row = (TableRow) node;
                        aBookToTransmit = ((Book) row.getItem()).getBook();
                        NewAbstractBookAuthorController.clearLists();
                        NewAbstractBookAuthorController.getAbstractBooks().add(aBookToTransmit);
                        NewAbstractBookAuthorController.getAuthors().addAll(aBookToTransmit.getAuthors());
                        getAbstractBookStage().showAndWait();
                    }
                }
            }
        });
    }

    private void setCellValues() {
        abstractBookColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Book, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Book, String> data) {
                return new SimpleStringProperty(data.getValue().getBook().getTitle());
            }
        });

        pagesQuantityColumn.setCellValueFactory(new PropertyValueFactory<Book, Integer>("pagesQuantity"));

        isGivenColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Book, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Book, String> data) {
                if (data.getValue().isGiven().toString().equals("true")) {
                    return new SimpleStringProperty("Yes");
                } else {
                    return new SimpleStringProperty("No");
                }
            }
        });

        publishingHouseColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("publishingHouse"));

        languageColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("language"));

        isbnColumn.setCellValueFactory(new PropertyValueFactory<Book, Integer>("isbn"));
    }

    public void initBooksInLibrary() throws  IOException, ClassNotFoundException {
        booksInLibrary.clear();

        List<Book> books=null;

        try {
            books=(List<Book>) TaskTransaction.run("Book", "getAll");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        for (Book book : books) {
            booksInLibrary.add(book);
        }
    }

    private void showAllBooks() throws IOException, ClassNotFoundException {
        initBooksInLibrary();
        dataTableView.setItems(booksInLibrary);

    }

    public void searchButtonClicked() throws IOException {
        getModalStage().showAndWait();

        if (isSearchAndUpdatedResultReturned) {
            dataTableView.setItems(searchAndUpdatedResult);
        }

        isSearchAndUpdatedResultReturned = false;
    }

    public void clearButtonClicked() throws IOException, ClassNotFoundException {
        initBooksInLibrary();
        dataTableView.setItems(booksInLibrary);
    }

    public void importButtonClicked() throws IOException,ClassNotFoundException {
        configureFileChooser("Open XML file.", "XML", "*.xml");
        File file =
                fileChooser.showOpenDialog(getLibraryStage());

        try {
            if ((file != null)) {
               TaskTransaction.run("import", (Object) readFile(file.getAbsolutePath()));
            }
        }  catch (NullPointerException e) {
        } catch (ClassNotFoundException e) {}

        showAllBooks();
    }

    private String readFile( String filePath ) throws IOException {
        BufferedReader reader = new BufferedReader( new FileReader (filePath));
        String         line;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        while( ( line = reader.readLine() ) != null ) {
            stringBuilder.append( line );
            stringBuilder.append( ls );
        }

        return stringBuilder.toString();
    }

    public void exportButtonClicked() throws JAXBException, IOException, SQLException {
        configureFileChooser("Save XML file.");
        File file = fileChooser.showSaveDialog(getLibraryStage());
        if (file != null) {
            String fileContext="";
            try {
                fileContext = (String) TaskTransaction.run("", "export");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            PrintWriter writer = new PrintWriter(file.getPath());
            writer.write(fileContext);
            writer.close();
        }
    }

    private void configureFileChooser(String windowTitle, String extensionName, String extension) {
        fileChooser.setTitle(windowTitle);
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(extensionName, extension));
    }

    private void configureFileChooser(String windowTitle) {
        fileChooser.setTitle(windowTitle);
        fileChooser.setInitialDirectory(new File("C:\\"));
    }

    public void addButtonClicked() {
        getAddBookStage().showAndWait();

        refreshTable();
    }

    public void updateButtonClicked() {
        if (!dataTableView.getSelectionModel().isEmpty()) {
            UpdateBookController.setSelectedBook(dataTableView.getSelectionModel().getSelectedItem());
            getUpdateBookStage().showAndWait();

            refreshTable();
        } else {
            createErrorDialog("Select the book!");
        }
    }

    public void deleteButtonClicked() {
        DeleteController.setBook(dataTableView.getSelectionModel().getSelectedItem());
        getDeleteStage().showAndWait();

        refreshTable();
    }

    private void createErrorDialog(String errorMessage) {
        Alert alert = new Alert(Alert.AlertType.ERROR, errorMessage, ButtonType.OK);
        alert.initModality(Modality.WINDOW_MODAL);
        alert.setTitle("Error!");
        alert.showAndWait();
    }

    private void refreshTable(){
        if (isSearchAndUpdatedResultReturned) {
            dataTableView.setItems(searchAndUpdatedResult);
        }

        isSearchAndUpdatedResultReturned = false;
    }
}