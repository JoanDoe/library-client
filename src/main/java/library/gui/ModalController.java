package library.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
//import library.dao.impl.BookDaoImpl;
//import library.dao.controller.SearchCriteria;
import library.TaskTransaction;
import library.dao.util.SearchCriteria;
import library.model.Book;

import java.io.IOException;
import java.net.URL;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

public class ModalController implements Initializable {

    @FXML
    private static Stage modalStage;
    @FXML
    private static Stage libraryStage;

    @FXML
    private BorderPane borderPane;
    @FXML
    private DatePicker publicationDateFrom;
    @FXML
    private DatePicker publicationDateTo;
    @FXML
    private DatePicker birthDateFrom;
    @FXML
    private DatePicker birthDateTo;
    @FXML
    private DatePicker deathDateFrom;
    @FXML
    private DatePicker deathDateTo;
    @FXML
    private ComboBox amongGivenComboBox;
    @FXML
    private GridPane book;

    @FXML
    private GridPane author;

    private SearchCriteria criteria = new SearchCriteria();

    public static Stage getModalStage() {
        return modalStage;
    }

    public static void setModalStage(Stage modalStage) {
        ModalController.modalStage = modalStage;
    }

    public static Stage getLibraryStage() {
        return libraryStage;
    }

    public static void setLibraryStage(Stage libraryStage) {
        ModalController.libraryStage = libraryStage;
    }

    @FXML
    public void initialize(URL url, ResourceBundle resourceBundle) {
        amongGivenComboBox.getItems().addAll(
                "isGiven",
                "isNotGiven",
                "all"
        );
        amongGivenComboBox.setValue("all");
    }

    public void OKButtonClicked() throws IOException, ClassNotFoundException {
        initializeHashMaps();
        showSearchResults(criteria);
        clearAllSearchAreas();
        getModalStage().hide();
    }

    public void cancelButtonClicked() {
        clearAllSearchAreas();
        getModalStage().hide();
    }

    private void clearAllSearchAreas() {
        for (Node node : borderPane.getChildren()) {
            if (node instanceof GridPane) {
                for (Node childNode : ((GridPane) node).getChildren()) {
                    if (childNode instanceof Label) {
                        continue;
                    }
                    if (childNode instanceof TextArea) {
                        ((TextArea) childNode).clear();
                        continue;
                    }
                    if (childNode instanceof DatePicker) {
                        ((DatePicker) childNode).setValue(null);
                        continue;
                    }
                    if (childNode instanceof ComboBox) {
                        ((ComboBox) childNode).getSelectionModel().clearSelection();
                        continue;
                    }
                }
            }
        }
        amongGivenComboBox.setValue("all");
    }

    private void initializeHashMaps() {
        criteria.setText(getTextFromTextArea());
        criteria.setDateFrom(getDateFrom());
        criteria.setDateTo(getDateTo());
        criteria.setIsGiven(amongGivenComboBox.getValue().toString());
    }

    private void showSearchResults(SearchCriteria criteria) throws IOException, ClassNotFoundException {
        //List<Book> searchAndUpdatedResult = bookDao.search(criteria);
        List<Book> searchResult = (List<Book>) TaskTransaction.run("search", (Object) criteria);
        LibraryController.isSearchAndUpdatedResultReturned = true;
        LibraryController.searchAndUpdatedResult.clear();
        LibraryController.searchAndUpdatedResult.addAll(searchResult);
    }

    private HashMap<String, String> getTextFromTextArea() {
        HashMap<String, String> textCriteria = new HashMap<String, String>();

        for (Node node : borderPane.getChildren()) {
            if (node instanceof GridPane) {
                for (Node textArea : ((GridPane) node).getChildren()) {
                    if ((textArea instanceof TextArea) && (((TextArea) textArea).getText().trim().length() != 0)) {
                        if (node.getId() != null) {
                            textCriteria.put(new String(node.getId() + '.' + textArea.getId()), ((TextArea) textArea).getText());
                        } else {
                            textCriteria.put(textArea.getId(), ((TextArea) textArea).getText());
                        }
                    }
                }
            }
        }

        return textCriteria;
    }

    private HashMap<String, Date> getDateFrom() {
        HashMap<String, Date> dateCriteria = new HashMap<String, Date>();
        Date tempDate;

        if (publicationDateFrom.getValue() != null) {
            tempDate = Date.from((publicationDateFrom.getValue()).atStartOfDay(ZoneId.systemDefault()).toInstant());
            dateCriteria.put("book.publicationDate", tempDate);
        }

        if (birthDateFrom.getValue() != null) {
            tempDate = Date.from((birthDateFrom.getValue()).atStartOfDay(ZoneId.systemDefault()).toInstant());
            dateCriteria.put("author.birthDate", tempDate);
        }

        if (deathDateFrom.getValue() != null) {
            tempDate = Date.from((deathDateFrom.getValue()).atStartOfDay(ZoneId.systemDefault()).toInstant());
            dateCriteria.put("author.deathDate", tempDate);
        }
        return dateCriteria;
    }

    private HashMap<String, Date> getDateTo() {
        HashMap<String, Date> dateCriteria = new HashMap<String, Date>();
        Date tempDate;

        if (publicationDateTo.getValue() != null) {
            tempDate = Date.from((publicationDateTo.getValue()).atStartOfDay(ZoneId.systemDefault()).toInstant());
            dateCriteria.put("book.publicationDate", tempDate);
        }

        if (birthDateTo.getValue() != null) {
            tempDate = Date.from((birthDateTo.getValue()).atStartOfDay(ZoneId.systemDefault()).toInstant());
            dateCriteria.put("author.birthDate", tempDate);
        }

        if (deathDateTo.getValue() != null) {
            tempDate = Date.from((deathDateTo.getValue()).atStartOfDay(ZoneId.systemDefault()).toInstant());
            dateCriteria.put("author.deathDate", tempDate);
        }
        return dateCriteria;
    }
}
