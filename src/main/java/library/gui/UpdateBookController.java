package library.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
//import library.dao.impl.AbstractBookDaoImpl;
//import library.dao.impl.BookDaoImpl;
import library.TaskTransaction;
import library.model.AbstractBook;
import library.model.Book;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

public class UpdateBookController implements Initializable {
	@FXML
	private static Stage updateStage;

	@FXML
	private static Stage updateAbstractBookStage;
	private static int bookId;
	private static Book selectedBook;
	@FXML
	private ComboBox bookComboBox;
	@FXML
	private TextField pagesTextField;
	@FXML
	private TextField publishingHouseFiled;
	@FXML
	private TextField languageField;
	@FXML
	private TextField isbnTextField;
	@FXML
	private CheckBox isGivenCheckBox;
	@FXML
	private BorderPane borderPane;

	private HashMap<String, Integer> bookMap = new HashMap<String, Integer>();

	public static Stage getUpdateStage() {
		return updateStage;
	}

	public static void setUpdateStage(Stage updateStage) {
		UpdateBookController.updateStage = updateStage;
	}

	public static int getBookId() {
		return bookId;
	}

	public static void setBookId(int bookId) {
		UpdateBookController.bookId = bookId;
	}

	public static Book getSelectedBook() {
		return selectedBook;
	}

	public static void setSelectedBook(Book selectedBook) {
		UpdateBookController.selectedBook = selectedBook;
	}

	public static Stage getUpdateAbstractBookStage() {
		return updateAbstractBookStage;
	}

	public static void setUpdateAbstractBookStage(Stage updateAbstractBookStage) {
		UpdateBookController.updateAbstractBookStage = updateAbstractBookStage;
	}

	@FXML
	public void initialize(URL url, ResourceBundle resourceBundle) {
		setComboBoxItems();
	}

	public void setComboBoxItems() {
		bookMap.clear();
		bookComboBox.getItems().clear();
		try {
			//List<AbstractBook> books = abstractBookDao.getAll();
			List<AbstractBook> books = (List<AbstractBook>) TaskTransaction.run("AbstractBook", "getAll");

			for (AbstractBook book : books) {
				bookMap.put(book.getTitle(), book.getId());
			}
			bookComboBox.getItems().addAll(bookMap.keySet());

			if (bookMap.keySet().iterator().hasNext()) {
				bookComboBox.setValue(bookMap.keySet().iterator().next());
			}
		}  catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void updateBookButtonClicked() throws SQLException {
		UpdateAbstractBookController.setSelectedBook(selectedBook.getBook());
		updateAbstractBookStage.showAndWait();
		setComboBoxItems();
	}

	private void setBookData() {
		bookComboBox.setValue(bookComboBox.getItems().get(0));
	}

	public void OKButtonClicked() throws IOException, ClassNotFoundException {

		//Book book = bookDao.get(selectedBook.getId());
		Book book = (Book) TaskTransaction.run("Book", "get", selectedBook.getId());

		if ((bookComboBox.getSelectionModel().isEmpty()) || (isbnTextField.getText().equals(""))) {
			createErrorDialog("A book should have an abstract book and an isbn!");
		} else {
			//book.setBook(abstractBookDao.get(bookMap.get(bookComboBox.getValue().toString())));
			book.setBook((AbstractBook) TaskTransaction.run("AbstractBook", "get", bookMap.get(bookComboBox.getValue().toString())));

			if (!pagesTextField.getText().equals("")) {
				book.setPagesQuantity(Integer.parseInt(pagesTextField.getText()));
			}
			if (!publishingHouseFiled.getText().equals("")) {
				book.setPublishingHouse(publishingHouseFiled.getText());
			}
			if (!languageField.getText().equals("")) {
				book.setLanguage(languageField.getText());
			}

			book.setIsbn(isbnTextField.getText());

			if (!isGivenCheckBox.getText().equals("")) {
				book.setIsGiven(isGivenCheckBox.isSelected());
			}

			//bookDao.saveOrUpdate(book);
			TaskTransaction.run("Book", "saveOrUpdate", book);

			showUpdatedBooks();

			getUpdateStage().hide();
			clearAllAreas();
		}
	}

	private void clearAllAreas() {
		for (Node node : borderPane.getChildren()) {
			if (node instanceof VBox) {
				for (Node childNode : ((VBox) node).getChildren()) {
					if (childNode instanceof Label) {
						continue;
					}
					if (childNode instanceof TextField) {
						((TextField) childNode).clear();
						continue;
					}
					if (childNode instanceof ComboBox) {
						((ComboBox) childNode).getSelectionModel().clearSelection();
						continue;
					}
				}
			}
		}
	}

	public void cancelButtonClicked() {
		getUpdateStage().hide();
		clearAllAreas();
	}

	private void createErrorDialog(String errorMessage) {
		Alert alert = new Alert(Alert.AlertType.ERROR, errorMessage, ButtonType.OK);
		alert.initModality(Modality.WINDOW_MODAL);
		alert.setTitle("Error!");
		alert.showAndWait();
	}

	private void showUpdatedBooks() throws IOException, ClassNotFoundException {
		List<Book> updatedBooks = (List<Book>) TaskTransaction.run("Book", "getAll");
		LibraryController.isSearchAndUpdatedResultReturned = true;
		LibraryController.searchAndUpdatedResult.clear();
		LibraryController.searchAndUpdatedResult.addAll(updatedBooks);
	}
}


