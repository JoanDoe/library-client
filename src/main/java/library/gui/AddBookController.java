package library.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import library.TaskTransaction;
import library.model.AbstractBook;
import library.model.Book;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

//import library.dao.impl.AbstractBookDaoImpl;
//import library.dao.impl.AuthorDaoImpl;
//import library.dao.impl.BookDaoImpl;

public class AddBookController implements Initializable {
    @FXML
    private static Stage addAbstractBookStage;

    @FXML
    private static Stage addBookStage;

    @FXML
    private ComboBox bookComboBox;
    @FXML
    private TextField pagesTextField;
    @FXML
    private TextField publishingHouseFiled;
    @FXML
    private TextField languageField;
    @FXML
    private TextField isbnTextField;
    @FXML
    private CheckBox isGivenCheckBox;
    @FXML
    private BorderPane borderPane;

    private HashMap<String, Integer> bookMap = new HashMap<String, Integer>();

    public static Stage getAddAbstractBookStage() {
        return addAbstractBookStage;
    }

    public static void setAddAbstractBookStage(Stage addAbstractBookStage) {
        AddBookController.addAbstractBookStage = addAbstractBookStage;
    }

    public static Stage getAddBookStage() {
        return addBookStage;
    }

    public static void setAddBookStage(Stage addBookStage) {
        AddBookController.addBookStage = addBookStage;
    }


    @FXML
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setComboBoxItems();
    }

    private void setComboBoxItems() {
        bookMap.clear();
        bookComboBox.getItems().clear();

        try {
            List<AbstractBook> books = (List<AbstractBook>) TaskTransaction.run("AbstractBook", "getAll");
            for (AbstractBook book : books) {
                bookMap.put(book.getTitle(), book.getId());
            }
            bookComboBox.getItems().addAll(bookMap.keySet());

            if (bookMap.keySet().iterator().hasNext()) {
                bookComboBox.setValue(bookMap.keySet().iterator().next());
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void OKButtonClicked() throws SQLException, IOException, ClassNotFoundException {
        Book newBook = new Book();

        if ((bookComboBox.getSelectionModel().isEmpty()) || (isbnTextField.getText().equals(""))) {
            createErrorDialog("A book should have an abstract book and an isbn!");
        } else {
            newBook.setBook((AbstractBook) TaskTransaction.run("AbstractBook", "get", (Object) bookMap.get(bookComboBox.getValue().toString())));

            if (!pagesTextField.getText().equals("")) {
                newBook.setPagesQuantity(Integer.parseInt(pagesTextField.getText()));
            }
            if (!publishingHouseFiled.getText().equals("")) {
                newBook.setPublishingHouse(publishingHouseFiled.getText());
            }
            if (!languageField.getText().equals("")) {
                newBook.setLanguage(languageField.getText());
            }

            newBook.setIsbn(isbnTextField.getText());

            newBook.setIsGiven(isGivenCheckBox.isSelected());
            //bookDao.add(newBook);
            TaskTransaction.run("Book", "add", newBook);

            showUpdatedBooks();

            getAddBookStage().hide();
            clearAllAreas();
        }
    }


    private void clearAllAreas() {
        for (Node node : borderPane.getChildren()) {
            if (node instanceof VBox) {
                for (Node childNode : ((VBox) node).getChildren()) {
                    if (childNode instanceof Label) {
                        continue;
                    }
                    if (childNode instanceof TextField) {
                        ((TextField) childNode).clear();
                        continue;
                    }
                    if (childNode instanceof ComboBox) {
                        ((ComboBox) childNode).getSelectionModel().clearSelection();
                        continue;
                    }
                }
            }
        }
    }

    public void addBookButtonClicked() {
        getAddAbstractBookStage().showAndWait();
        setComboBoxItems();
    }

    public void cancelButtonClicked() {
        getAddBookStage().hide();
        clearAllAreas();
    }

    private void createErrorDialog(String errorMessage) {
        Alert alert = new Alert(Alert.AlertType.ERROR, errorMessage, ButtonType.OK);
        alert.initModality(Modality.WINDOW_MODAL);
        alert.setTitle("Error!");
        alert.showAndWait();
    }

    private void showUpdatedBooks() throws IOException, ClassNotFoundException {
        List<Book> updatedBooks = (List<Book>) TaskTransaction.run("Book", "getAll");
        LibraryController.isSearchAndUpdatedResultReturned = true;
        LibraryController.searchAndUpdatedResult.clear();
        LibraryController.searchAndUpdatedResult.addAll(updatedBooks);
    }
}


