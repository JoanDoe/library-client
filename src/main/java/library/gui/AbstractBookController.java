package library.gui;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import library.model.AbstractBook;
import library.model.Author;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Set;

public class AbstractBookController implements Initializable {

    @FXML
    private static Stage authorStage;

    @FXML
    private static Stage addAbstractBookStage;

    @FXML
    private static Stage updateAbstractBookStage;

    private static ObservableList<AbstractBook> abstractBooks = FXCollections.observableArrayList();

    private static Set<Author> authorToTransmit;
    @FXML
    private TableView<AbstractBook> abstractTable;
    @FXML
    private TableColumn<AbstractBook, String> titleColumn;
    @FXML
    private TableColumn<AbstractBook, String> publicationDateColumn;
    @FXML
    private TableColumn<AbstractBook, String> genreColumn;

    public static Stage getAuthorStage() {
        return authorStage;
    }

    public static void setAuthorStage(Stage authorStage) {
        AbstractBookController.authorStage = authorStage;
    }

    public static ObservableList<AbstractBook> getAbstractBooks() {
        return abstractBooks;
    }

    public static void setAbstractBooks(ObservableList<AbstractBook> abstractBooks) {
        AbstractBookController.abstractBooks = abstractBooks;
    }

    public static Stage getAddAbstractBookStage() {
        return addAbstractBookStage;
    }

    public static void setAddAbstractBookStage(Stage addAbstractBookStage) {
        AbstractBookController.addAbstractBookStage = addAbstractBookStage;
    }

    public static Stage getUpdateAbstractBookStage() {
        return updateAbstractBookStage;
    }

    public static void setUpdateAbstractBookStage(Stage updateAbstractBookStage) {
        AbstractBookController.updateAbstractBookStage = updateAbstractBookStage;
    }

    @FXML
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setCellValues();
        showAbstractBook();

        abstractTable.setOnMousePressed(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                    Node node = ((Node) event.getTarget()).getParent();
                    TableRow row;
                    if ((node instanceof TableRow) && (!((TableRow) node).isEmpty())) {
                        row = (TableRow) node;
                        authorToTransmit = ((AbstractBook) row.getItem()).getAuthors();
                        AuthorController.getAuthors().clear();
                        AuthorController.getAuthors().addAll(authorToTransmit);
                        getAuthorStage().showAndWait();
                    }
                }
            }
        });
    }

    private void setCellValues() {
        titleColumn.setCellValueFactory(new PropertyValueFactory<AbstractBook, String>("title"));

        publicationDateColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<AbstractBook, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(final TableColumn.CellDataFeatures<AbstractBook, String> data) {
                return new SimpleStringProperty(getDate(data.getValue().getPublicationDate()));
            }
        });

        genreColumn.setCellValueFactory(new PropertyValueFactory<AbstractBook, String>("genre"));
    }

    private String getDate(Date date) {
        if (date == null) {
            return "";
        } else {
            return date.toString();
        }
    }

    private void showAbstractBook() {
        abstractTable.setItems(getAbstractBooks());
    }
}
