package library.gui;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import library.model.Author;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

public class AuthorController implements Initializable {

    private static ObservableList<Author> authors = FXCollections.observableArrayList();
    @FXML
    private TableView<Author> authorTable;
    @FXML
    private TableColumn<Author, String> firstNameColumn;
    @FXML
    private TableColumn<Author, String> lastNameColumn;
    @FXML
    private TableColumn<Author, String> birthDateColumn;
    @FXML
    private TableColumn<Author, String> deathDateColumn;
    @FXML
    private TableColumn<Author, String> nationalityColumn;

    public static ObservableList<Author> getAuthors() {
        return authors;
    }

    public static void setAuthors(ObservableList<Author> authors) {
        AuthorController.authors = authors;
    }

    @FXML
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setCellValues();
        showAuthors();
    }

    private void setCellValues() {
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<Author, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Author, String>("lastName"));
        birthDateColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Author, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Author, String> data) {
                return new SimpleStringProperty(getDate(data.getValue().getBirthDate()));
            }
        });
        deathDateColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Author, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Author, String> data) {
                return new SimpleStringProperty(getDate(data.getValue().getDeathDate()));
            }
        });
        nationalityColumn.setCellValueFactory(new PropertyValueFactory<Author, String>("nationality"));
    }

    private String getDate(Date date) {
        if (date == null) {
            return "";
        } else {
            return date.toString();
        }
    }

    private void showAuthors() {
        getAuthors().clear();
        authorTable.setItems(getAuthors());
    }
}
