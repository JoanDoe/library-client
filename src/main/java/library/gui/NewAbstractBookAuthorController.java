package library.gui;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import library.model.AbstractBook;
import library.model.Author;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.stream.IntStream;

public class NewAbstractBookAuthorController implements Initializable {
	@FXML
	private TableView<AbstractBook> abstractBookTableView;

	@FXML
	private TableView<Author> authorTableView;

	@FXML
	private TableColumn<AbstractBook, String> titleColumn;
	@FXML
	private TableColumn<AbstractBook, String> publicationDateColumn;
	@FXML
	private TableColumn<AbstractBook, String> genreColumn;
	@FXML
	private TableColumn<Author, String> firstNameColumn;
	@FXML
	private TableColumn<Author, String> lastNameColumn;
	@FXML
	private TableColumn<Author, String> birthDateColumn;
	@FXML
	private TableColumn<Author, String> deathDateColumn;
	@FXML
	private TableColumn<Author, String> nationalityColumn;

	@FXML
	private static Stage NewAbstractBookStage;

	private static ObservableList<AbstractBook> abstractBooks = FXCollections.observableArrayList();

	private static ObservableList<Author> authors = FXCollections.observableArrayList();

	public static ObservableList<AbstractBook> getAbstractBooks() {
		return abstractBooks;
	}

	public static void setAbstractBooks(ObservableList<AbstractBook> abstractBooks) {
		NewAbstractBookAuthorController.abstractBooks = abstractBooks;
	}

	public static ObservableList<Author> getAuthors() {
		return authors;
	}

	public static void setAuthors(ObservableList<Author> authors) {
		NewAbstractBookAuthorController.authors = authors;
	}

	public static Stage getNewAbstractBookStage() {
		return NewAbstractBookStage;
	}

	public static void setNewAbstractBookStage(Stage newAbstractBookStage) {
		NewAbstractBookStage = newAbstractBookStage;
	}

	@FXML
	public void initialize(URL url, ResourceBundle resourceBundle) {
		setAbstractBookCellValues();
		setAuthorCellValues();

		abstractBookTableView.setFixedCellSize(25);
		abstractBookTableView.prefHeightProperty().bind(abstractBookTableView.fixedCellSizeProperty().multiply(Bindings.size(abstractBookTableView.getItems()).add(2.01)));
		abstractBookTableView.minHeightProperty().bind(abstractBookTableView.prefHeightProperty());
		abstractBookTableView.maxHeightProperty().bind(abstractBookTableView.prefHeightProperty());

		showAbstractBook();
		showAuthors();
	}

	private void setAbstractBookCellValues() {
		firstNameColumn.setCellValueFactory(new PropertyValueFactory<Author, String>("firstName"));
		lastNameColumn.setCellValueFactory(new PropertyValueFactory<Author, String>("lastName"));
		birthDateColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Author, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(TableColumn.CellDataFeatures<Author, String> data) {
				return new SimpleStringProperty(getDate(data.getValue().getBirthDate()));
			}
		});
		deathDateColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Author, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(TableColumn.CellDataFeatures<Author, String> data) {
				return new SimpleStringProperty(getDate(data.getValue().getDeathDate()));
			}
		});
		nationalityColumn.setCellValueFactory(new PropertyValueFactory<Author, String>("nationality"));
		titleColumn.setCellValueFactory(new PropertyValueFactory<AbstractBook, String>("title"));

		publicationDateColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<AbstractBook, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(final TableColumn.CellDataFeatures<AbstractBook, String> data) {
				return new SimpleStringProperty(getDate(data.getValue().getPublicationDate()));
			}
		});

		genreColumn.setCellValueFactory(new PropertyValueFactory<AbstractBook, String>("genre"));
	}

	private void setAuthorCellValues() {
		firstNameColumn.setCellValueFactory(new PropertyValueFactory<Author, String>("firstName"));
		lastNameColumn.setCellValueFactory(new PropertyValueFactory<Author, String>("lastName"));
		birthDateColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Author, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(TableColumn.CellDataFeatures<Author, String> data) {
				return new SimpleStringProperty(getDate(data.getValue().getBirthDate()));
			}
		});
		deathDateColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Author, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(TableColumn.CellDataFeatures<Author, String> data) {
				return new SimpleStringProperty(getDate(data.getValue().getDeathDate()));
			}
		});
		nationalityColumn.setCellValueFactory(new PropertyValueFactory<Author, String>("nationality"));
	}

	private String getDate(Date date) {
		if (date == null) {
			return "";
		} else {
			return date.toString();
		}
	}

	private void showAbstractBook() {
		abstractBookTableView.setItems(getAbstractBooks());
	}

	private void showAuthors() {
		authorTableView.setItems(getAuthors());
	}

	public static void clearLists() {
		getAuthors().clear();
		getAbstractBooks().clear();
	}

	public void OKButtonClicked() {
		getNewAbstractBookStage().hide();
	}
}
