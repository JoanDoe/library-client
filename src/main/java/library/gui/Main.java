package library.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
//import library.controller.HibernateUtil;

public class Main extends Application {
    private final static String STAGE_TITLE = "Library";

    private final static String GUI2_FXML = "gui2.fxml";
    private final static String MODAL_FXML = "modal.fxml";
    private final static String ABSTRACT_BOOK_FXML = "abstract_book.fxml";
    private final static String AUTHOR_FXML = "author.fxml";
    private final static String ADD_BOOK_FXML = "add_book.fxml";
    private final static String UPDATE_BOOK_FXML = "update_book.fxml";
    private final static String ADD_ABSTRACT_BOOK_FXML = "add_abstract_book.fxml";
    private final static String UPDATE_ABSTRACT_BOOK_FXML = "update_abstract_book.fxml";
    private final static String ADD_AUTHOR_FXML = "add_author.fxml";
    private final static String UPDATE_AUTHOR_FXML = "update_author.fxml";
    private final static String DELETE_FXML = "delete.fxml";
    private final static String ABSTRACT_BOOK_AUTHOR_FXML = "new_abstract_book_author.fxml";

    private final static float MIN_HEIGHT = 500;
    private final static float MIN_WIDTH = 600;

    private final static float MIN_MODAL_HEIGHT = 330;
    private final static float MIN_MODAL_WIDTH = 710;

//    private static BufferedOutputStream bos ;
//    private static ObjectOutputStream oos ;
//    private static Socket socket;
//    private static BufferedInputStream bis ;
//    private static ObjectInputStream ois ;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        InetAddress addr = InetAddress.getByName("127.0.0.1");
        System.out.println("addr = " + addr);
       // socket = new Socket(addr, 8081);

//        try {
//             bos = new BufferedOutputStream(socket.getOutputStream());
//             oos = new ObjectOutputStream(bos);
//             bis = new BufferedInputStream(socket.getInputStream());
//             ois = new ObjectInputStream(bis);
//        }
//        catch (Exception e){}
//        finally {
//            System.out.println("test");
//        }
        //main window
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource(GUI2_FXML));

        Scene scene = new Scene(root);

        primaryStage.setTitle(STAGE_TITLE);
        primaryStage.setMinHeight(MIN_HEIGHT);
        primaryStage.setMinWidth(MIN_WIDTH);
        primaryStage.getIcons().add(new Image("cat.png"));

       // LibraryController.streams = new

        primaryStage.setScene(scene);
        primaryStage.show();

        //modal search window
        Parent modal = FXMLLoader.load(getClass().getClassLoader().getResource(MODAL_FXML));

        Scene modalScene = new Scene(modal);

        Stage modalWindow = new Stage();

        modalWindow.initModality(Modality.APPLICATION_MODAL);
        modalWindow.setMaxWidth(MIN_MODAL_WIDTH);
        modalWindow.setMaxHeight(MIN_MODAL_HEIGHT);
        modalWindow.setMinHeight(MIN_MODAL_HEIGHT);
        modalWindow.setMinWidth(MIN_MODAL_WIDTH);
        modalWindow.setTitle("Search");

        LibraryController.setModalStage(modalWindow);
        ModalController.setModalStage(modalWindow);

        ModalController.setLibraryStage(primaryStage);
        LibraryController.setLibraryStage(primaryStage);

        modalWindow.setScene(modalScene);
        modalWindow.hide();

//        //abstract book
//
//        Parent abstract_book = FXMLLoader.load(getClass().getClassLoader().getResource(ABSTRACT_BOOK_FXML));
//
//        Scene abstractScene = new Scene(abstract_book);
//
//        Stage abstractWindow = new Stage();
//
//        abstractWindow.initModality(Modality.APPLICATION_MODAL);
//        abstractWindow.setTitle("Abstract book");
//
//        LibraryController.setAbstractBookStage(abstractWindow);
//
//        abstractWindow.setScene(abstractScene);
//        abstractWindow.hide();

//        //author
//
//        Parent author = FXMLLoader.load(getClass().getClassLoader().getResource(AUTHOR_FXML));
//
//        Scene authorScene = new Scene(author);
//
//        Stage authorWindow = new Stage();
//
//        authorWindow.initModality(Modality.APPLICATION_MODAL);
//        authorWindow.setTitle("Author");
//
//        AbstractBookController.setAuthorStage(authorWindow);
//
//        authorWindow.setScene(authorScene);
//        authorWindow.hide();

        //add

        Parent addBook = FXMLLoader.load(getClass().getClassLoader().getResource(ADD_BOOK_FXML));

        Scene addScene = new Scene(addBook);

        Stage addBookWindow = new Stage();

        addBookWindow.initModality(Modality.APPLICATION_MODAL);
        addBookWindow.setTitle("Add book");

        LibraryController.setAddBookStage(addBookWindow);
        AddBookController.setAddBookStage(addBookWindow);

        addBookWindow.setScene(addScene);
        addBookWindow.hide();

        //update

        Parent updateBook = FXMLLoader.load(getClass().getClassLoader().getResource(UPDATE_BOOK_FXML));

        Scene updateScene = new Scene(updateBook);

        Stage updateWindow = new Stage();

        updateWindow.initModality(Modality.APPLICATION_MODAL);
        updateWindow.setTitle("Update book");

        LibraryController.setUpdateBookStage(updateWindow);
        UpdateBookController.setUpdateStage(updateWindow);

        updateWindow.setScene(updateScene);
        updateWindow.hide();

        //add abstract book

        Parent addAbstractBook = FXMLLoader.load(getClass().getClassLoader().getResource(ADD_ABSTRACT_BOOK_FXML));

        Scene addAbstractBookScene = new Scene(addAbstractBook);

        Stage addAbstractWindow = new Stage();

        addAbstractWindow.initModality(Modality.APPLICATION_MODAL);
        addAbstractWindow.setTitle("Add abstract book");

        AddAbstractBookController.setAddAbstractBookStage(addAbstractWindow);
        AddBookController.setAddAbstractBookStage(addAbstractWindow);

        addAbstractWindow.setScene(addAbstractBookScene);
        addAbstractWindow.hide();

        //update abstract book

        Parent updateAbstractBook = FXMLLoader.load(getClass().getClassLoader().getResource(UPDATE_ABSTRACT_BOOK_FXML));

        Scene updateAbstractBookScene = new Scene(updateAbstractBook);

        Stage updateAbstractWindow = new Stage();

        updateAbstractWindow.initModality(Modality.APPLICATION_MODAL);
        updateAbstractWindow.setTitle("Update abstract book");

        UpdateBookController.setUpdateAbstractBookStage(updateAbstractWindow);
        UpdateAbstractBookController.setUpdateAuthorStage(updateAbstractWindow);

        updateAbstractWindow.setScene(updateAbstractBookScene);
        updateAbstractWindow.hide();

        //add author

        Parent addAuthor = FXMLLoader.load(getClass().getClassLoader().getResource(ADD_AUTHOR_FXML));

        Scene addAuthorScene = new Scene(addAuthor);

        Stage addAuthorWindow = new Stage();

        addAuthorWindow.initModality(Modality.APPLICATION_MODAL);
        addAuthorWindow.setTitle("Add author");

        AddAuthorController.setAddAuthorStage(addAuthorWindow);

        addAuthorWindow.setScene(addAuthorScene);
        addAuthorWindow.hide();

        //update author

        Parent updateAuthor = FXMLLoader.load(getClass().getClassLoader().getResource(UPDATE_AUTHOR_FXML));

        Scene updateAuthorScene = new Scene(updateAuthor);

        Stage updateAuthorWindow = new Stage();

        updateAuthorWindow.initModality(Modality.APPLICATION_MODAL);
        updateAuthorWindow.setTitle("Update author");

        UpdateAuthorController.setUpdateAuthorStage(updateAuthorWindow);

        updateAuthorWindow.setScene(updateAuthorScene);
        updateAuthorWindow.hide();

        //delete

        Parent delete = FXMLLoader.load(getClass().getClassLoader().getResource(DELETE_FXML));

        Scene deleleteScene = new Scene(delete);

        Stage deleteWindow = new Stage();

        deleteWindow.initModality(Modality.APPLICATION_MODAL);
        deleteWindow.setTitle("Delete window");

        DeleteController.setDeleteStage(deleteWindow);
        LibraryController.setDeleteStage(deleteWindow);


        deleteWindow.setScene(deleleteScene);
        deleteWindow.hide();

        //abstract book author

        Parent abstract_book_author = FXMLLoader.load(getClass().getClassLoader().getResource(ABSTRACT_BOOK_AUTHOR_FXML));

        Scene abstractBookAuthorScene = new Scene(abstract_book_author);

        Stage abstractBookAuthorWindow = new Stage();

        abstractBookAuthorWindow.initModality(Modality.APPLICATION_MODAL);
        abstractBookAuthorWindow.setTitle("Abstract book and authors");

        LibraryController.setAbstractBookStage(abstractBookAuthorWindow);
        NewAbstractBookAuthorController.setNewAbstractBookStage(abstractBookAuthorWindow);

        abstractBookAuthorWindow.setScene(abstractBookAuthorScene);
        abstractBookAuthorWindow.hide();
    }

    @Override
    public void stop() {
        System.out.println("Stage is closing");
        //HibernateUtil.getSessionFactory().close();
        try {
        }
        catch (Exception e){}
    }
}
