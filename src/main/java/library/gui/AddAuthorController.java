package library.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
//import library.dao.impl.AuthorDaoImpl;
import library.TaskTransaction;
import library.model.Author;

import java.io.IOException;
import java.net.URL;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

public class AddAuthorController implements Initializable {
	@FXML
	private static Stage addAuthorStage;

	@FXML
	private TextField firstNameTextField;
	@FXML
	private TextField lastNameHouseFiled;
	@FXML
	private TextField nationalityTextField;
	@FXML
	private DatePicker birthDatePicker;
	@FXML
	private DatePicker deathDatePicker;
	@FXML
	private BorderPane borderPane;

	public static Stage getAddAuthorStage() {
		return addAuthorStage;
	}

	public static void setAddAuthorStage(Stage addAuthorStage) {
		AddAuthorController.addAuthorStage = addAuthorStage;
	}

	@FXML
	public void initialize(URL url, ResourceBundle resourceBundle) {

	}

	public void OKButtonClicked() throws IOException, ClassNotFoundException {
		Author newAuthor = new Author();

		if ((firstNameTextField.getText().equals("")) || (lastNameHouseFiled.getText().equals(""))) {
			createErrorDialog("An author should have a full name!");
		} else {
			newAuthor.setFirstName(firstNameTextField.getText());

			newAuthor.setLastName(lastNameHouseFiled.getText());

			if (!nationalityTextField.getText().equals("")) {
				newAuthor.setNationality(nationalityTextField.getText());
			}
			if (birthDatePicker.getValue() != null) {
				newAuthor.setBirthDate(Date.from(birthDatePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
			}
			if (deathDatePicker.getValue() != null) {
				newAuthor.setDeathDate(Date.from(deathDatePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
			}

			//authorDao.add(newAuthor);
            TaskTransaction.run("Author", "add",(Object) newAuthor);
			getAddAuthorStage().hide();
			clearAllAreas();
		}
	}

	private void clearAllAreas() {
		for (Node node : borderPane.getChildren()) {
			if (node instanceof VBox) {
				for (Node childNode : ((VBox) node).getChildren()) {
					if (childNode instanceof Label) {
						continue;
					}
					if (childNode instanceof TextField) {
						((TextField) childNode).clear();
						continue;
					}
					if (childNode instanceof DatePicker) {
						((DatePicker) childNode).setValue(null);
						continue;
					}
				}
			}
		}
	}

	public void cancelButtonClicked() {
		getAddAuthorStage().hide();
		clearAllAreas();
	}

	private void createErrorDialog(String errorMessage) {
		Alert alert = new Alert(Alert.AlertType.ERROR, errorMessage, ButtonType.OK);
		alert.initModality(Modality.WINDOW_MODAL);
		alert.setTitle("Error!");
		alert.showAndWait();
	}
}


