package library.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;
import library.TaskTransaction;
import library.dao.util.SearchCriteria;
import library.model.Book;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

public class DeleteController implements Initializable {
    private static Book book;

    @FXML
    private static Stage deleteStage;

    @FXML
    private ComboBox optionComboBox;

    public static Book getBook() {
        return book;
    }

    public static void setBook(Book book) {
        DeleteController.book = book;
    }

    public static Stage getDeleteStage() {
        return deleteStage;
    }

    public static void setDeleteStage(Stage deleteStage) {
        DeleteController.deleteStage = deleteStage;
    }

    @FXML
    public void initialize(URL url, ResourceBundle resourceBundle) {
        optionComboBox.getItems().addAll(
                "Book",
                "Abstract book"
        );
        optionComboBox.setValue("Book");
    }

    public void deleteButtonClicked() throws IOException, ClassNotFoundException {
        if (optionComboBox.getValue().toString() == "Book") {
            //bookDao.delete(book);
            TaskTransaction.run("Book", "delete", book);
        } else {
            SearchCriteria searchCriteria = new SearchCriteria();
            HashMap<String, String> textCriteria = new HashMap<String, String>();
            searchCriteria.setIsGiven("all");

            textCriteria.put("book.title", book.getBook().getTitle());
            searchCriteria.setText(textCriteria);
            //List<Book> books = bookDao.search(searchCriteria);
            List<Book> books = (List<Book>) TaskTransaction.run("search", searchCriteria);

            for (Book book : books) {
                //bookDao.delete(book);
                TaskTransaction.run("Book", "delete", book);
            }
            //abstractBookDao.delete(book.getBook());
            TaskTransaction.run("AbstractBook", "delete", book.getBook());
        }

        showUpdatedBooks();

        optionComboBox.setValue("Book");
        getDeleteStage().hide();
    }

    public void cancelButtonClicked() {
        optionComboBox.setValue("Book");
        getDeleteStage().hide();
    }

    private void showUpdatedBooks() throws IOException, ClassNotFoundException {
        List<Book> updatedBooks = (List<Book>) TaskTransaction.run("Book", "getAll");
        LibraryController.isSearchAndUpdatedResultReturned = true;
        LibraryController.searchAndUpdatedResult.clear();
        LibraryController.searchAndUpdatedResult.addAll(updatedBooks);
    }
}
