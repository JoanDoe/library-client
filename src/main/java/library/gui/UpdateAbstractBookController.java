package library.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
//import library.dao.impl.AbstractBookDaoImpl;
//import library.dao.impl.AuthorDaoImpl;
import library.TaskTransaction;
import library.model.AbstractBook;
import library.model.Author;

import java.io.IOException;
import java.net.URL;
import java.time.ZoneId;
import java.util.*;

public class UpdateAbstractBookController implements Initializable {
	@FXML
	private static Stage updateAuthorStage;
	private static int abstractBookId;
	private static AbstractBook selectedBook;
	ObservableList<String> authorsList = FXCollections.observableArrayList();
	@FXML
	private ListView<String> authorsListView;
	@FXML
	private TextField titleTextField;
	@FXML
	private DatePicker publicationDatePicker;
	@FXML
	private TextField genreTextField;
	@FXML
	private BorderPane borderPane;

	private HashMap<String, Integer> authorMap = new HashMap<String, Integer>();

	public static int getAbstractBookId() {
		return abstractBookId;
	}

	public static void setAbstractBookId(int abstractBookId) {
		UpdateAbstractBookController.abstractBookId = abstractBookId;
	}

	public static AbstractBook getSelectedBook() {
		return selectedBook;
	}

	public static void setSelectedBook(AbstractBook selectedBook) {
		UpdateAbstractBookController.selectedBook = selectedBook;
	}

	public static Stage getUpdateAuthorStage() {
		return updateAuthorStage;
	}

	public static void setUpdateAuthorStage(Stage updateAuthorStage) {
		UpdateAbstractBookController.updateAuthorStage = updateAuthorStage;
	}

	@FXML
	public void initialize(URL url, ResourceBundle resourceBundle) {
		authorsListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		setComboBoxItems();
	}

	private void setComboBoxItems() {
		authorMap.clear();
		authorsList.clear();
		authorsListView.getItems().clear();

		try {
			//List<Author> authors = authorDao.getAll();
            List<Author> authors = (List<Author>) TaskTransaction.run("Author", "getAll");
			for (Author author : authors) {
				authorMap.put(author.getLastName(), author.getId());
				authorsList.add(author.getLastName());
			}
			authorsListView.setItems(authorsList);
		} catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public void OKButtonClicked() throws IOException, ClassNotFoundException {

		//AbstractBook newAbstractBook = abstractBookDao.get(selectedBook.getId());
        AbstractBook newAbstractBook = (AbstractBook) TaskTransaction.run("AbstractBook", "get", selectedBook.getId());
		Set<Author> authorSet = new HashSet<Author>();

		if ((!(authorsListView.getSelectionModel().getSelectedItems().size() > 0)) || (titleTextField.getText().equals(""))) {
			createErrorDialog("A book should have an author and a title!");
		} else {
			for (String s : authorsListView.getSelectionModel().getSelectedItems()) {
				//authorSet.add(authorDao.get(authorMap.get(s)));
                authorSet.add((Author) TaskTransaction.run("Author", "get", authorMap.get(s)));
			}

			newAbstractBook.setTitle(titleTextField.getText());

			if (!genreTextField.getText().equals("")) {
				newAbstractBook.setGenre(genreTextField.getText());
			}
			if (publicationDatePicker.getValue() != null)
				newAbstractBook.setPublicationDate(Date.from(publicationDatePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));

			//abstractBookDao.saveOrUpdate(newAbstractBook);
            TaskTransaction.run("AbstractBook", "saveOrUpdate", newAbstractBook);

			getUpdateAuthorStage().hide();
			clearAllAreas();
		}
	}

	private void clearAllAreas() {
		for (Node node : borderPane.getChildren()) {
			if (node instanceof VBox) {
				for (Node childNode : ((VBox) node).getChildren()) {
					if (childNode instanceof Label) {
						continue;
					}
					if (childNode instanceof TextField) {
						((TextField) childNode).clear();
						continue;
					}
					if (childNode instanceof ComboBox) {
						((ComboBox) childNode).getSelectionModel().clearSelection();
						continue;
					}
				}
			}
		}
	}

	public void cancelButtonClicked() {
		getUpdateAuthorStage().hide();
		clearAllAreas();
	}

	public void updateAuthorButtonClicked() {
		UpdateAuthorController.getUpdateAuthorStage().showAndWait();
		setComboBoxItems();
	}

	private void createErrorDialog(String errorMessage) {
		Alert alert = new Alert(Alert.AlertType.ERROR, errorMessage, ButtonType.OK);
		alert.initModality(Modality.WINDOW_MODAL);
		alert.setTitle("Error!");
		alert.showAndWait();
	}
}


