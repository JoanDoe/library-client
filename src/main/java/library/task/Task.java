package library.task;

import java.io.Serializable;

public class Task implements Serializable {
	private String className;
	private String methodName;
	private Object param;

	public Task() {	}

	public Task(String className, String methodName) {
		this.className=className;
		this.methodName=methodName;
	}

	public Task(String className, String methodName, Object params) {
		this.methodName=methodName;
		this.className=className;
		this.param =params;
	}

	public Task(String methodName, Object params) {
		this.methodName=methodName;
		this.param =params;
	}


	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Object getParam() {
		return param;
	}

	public void setParam(Object param) {
		this.param = param;
	}
}
