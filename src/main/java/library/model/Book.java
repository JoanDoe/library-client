package library.model;


import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;


@Entity
@Table(name = "Books")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "book")
/**
 *  Book is the class representing specific material book.
 *  @see AbstractBook
 */
public class Book implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    /**
     * A variable representing unique identifier of record.
     */
    @XmlAttribute(name = "id")
    private int id;


    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "abstract_book_id")
    @XmlElement(name = "abstractBook")
    /**
     * A variable representing instance of AbstractBook.
     */
    private AbstractBook book;

    @Column(name = "pages_quantity")
    /**
     * A variable representing pages quantity.
     */
    private int pagesQuantity;

    @Column(name = "is_given")
    /**
     * A variable representing if book is given for somebody.
     */
    private boolean isGiven;

    @Column(name = "publishing_house")
    /**
     * A variable representing the name of publishing house.
     */
    private String publishingHouse;

    @Column(name = "language")
    /**
     * A variable representing the language in which book was written of translated.
     */
    private String language;

    @Column(name = "isbn")
    /**
     * A variable representing the isbn of book.
     */
    private String isbn;

    /**
     * The method used for getting value of if field.
     *
     * @return the int value contains id
     */
    public int getId() {
        return id;
    }

    /**
     * The method used for setting value of id field.
     *
     * @param id the value of id field which will be set.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * The method used for getting value of abstractBook field.
     *
     * @return the instance of AbstractBook class
     */
    public AbstractBook getBook() {
        return book;
    }

    /**
     * The method used for setting value of book field.
     *
     * @param book the value of book field which will be set.
     */
    public void setBook(AbstractBook book) {
        this.book = book;
    }


    /**
     * The method used for getting value of isGiven field.
     *
     * @return the boolean value representing if the book is given
     */
    public Boolean isGiven() {
        return isGiven;
    }

    /**
     * The method used for setting value of isGiven field.
     *
     * @param isGiven the value of isGiven field which will be set.
     */
    public void setIsGiven(boolean isGiven) {
        this.isGiven = isGiven;
    }

    /**
     * The method used for getting value of publishingHouse field.
     *
     * @return the string contains name of publishing house
     */
    public String getPublishingHouse() {
        return publishingHouse;
    }

    /**
     * The method used for setting value of publishingHouse field.
     *
     * @param publishingHouse the value of publishingHouse field which will be set.
     */
    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    /**
     * The method used for getting value of pagesQuantity field.
     *
     * @return the int value contains pages quantity
     */
    public int getPagesQuantity() {
        return pagesQuantity;
    }

    /**
     * The method used for setting value of pagesQuantity field.
     *
     * @param pagesQuantity the value of pagesQuantity field which will be set.
     */
    public void setPagesQuantity(int pagesQuantity) {
        this.pagesQuantity = pagesQuantity;
    }

    /**
     * The method used for getting value of language field.
     *
     * @return the string contains book language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * The method used for setting value of language field.
     *
     * @param language the value of language field which will be set.
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * The method used for getting value of isbn field.
     *
     * @return the int value contains isbn
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * The method used for setting value of isbn field.
     *
     * @param isbn the value of isbn field which will be set.
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    /**
     * The method used for equals instance of this class object and other object.
     *
     * @param object the object for comparing.
     * @return boolean value
     */
    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }

        if (object != null) {
            if (object instanceof Book) {
                Book other = (Book) object;
                if (((Book) object).getClass().getSimpleName().equals(this.getClass().getSimpleName())
                        & (other.isbn.toLowerCase().equals(this.isbn.toLowerCase()))) {
                    return true;
                }
                return false;
            }
            return false;
        }
        return false;

    }
}
