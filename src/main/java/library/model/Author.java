package library.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "author")
@Entity
@Table(name = "authors")
/**
 * Author is the class representing an author of literary work.
 */
public class Author implements Serializable {
    @Id
    @Column(name = "author_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    /**
     * A variable representing unique identifier of record.
     */
    @XmlAttribute(name = "id")
    private int id;


    @Column(name = "first_name")
    /**
     * A variable representing first name of author.
     */
    private String firstName;

    @Column(name = "last_name")
    /**
     * A variable representing last name of author.
     */
    private String lastName;


    @Column(name = "birth_date")
    /**
     * A variable representing author birth date.
     */
    private Date birthDate;

    @Column(name = "death_date")
    /**
     * A variable representing author death date.
     */
    private Date deathDate;

    @Column(name = "nationality")
    /**
     * A variable representing author nationality.
     */
    private String nationality;

    /**
     * The method used for getting unique identifier.
     *
     * @return the int value contains id
     */
    public int getId() {
        return id;
    }

    /**
     * The method used for setting value of id field.
     *
     * @param id the value of id field which will be set.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * The method used for getting value of firstName field.
     *
     * @return the string contains author first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * The method used for setting value of firstName field.
     *
     * @param firstName the value of firstName field which will be set.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * The method used for getting value of lastName field.
     *
     * @return the string contains author last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * The method used for setting value of lastName field.
     *
     * @param lastName the value of lastName field which will be set.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * The method used for getting value of birthDate field.
     *
     * @return the instance of Date class contains author birth date
     */

    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * The method used for setting value of birthDate field.
     *
     * @param birthDate the value of birthDate field which will be set.
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * The method used for getting value of deathDate field.
     *
     * @return the instance of Date class contains author death date
     */
    public Date getDeathDate() {
        return deathDate;
    }

    /**
     * The method used for setting value of deathDate field.
     *
     * @param deathDate the value of deathDate field which will be set.
     */
    public void setDeathDate(Date deathDate) {
        this.deathDate = deathDate;
    }

    /**
     * The method used for getting value of nationality field.
     *
     * @return the string contains author nationality
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * The method used for setting value of nationality field.
     *
     * @param nationality the value of nationality field which will be set.
     */
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    /**
     * The method used for equals instance of this class object and other object.
     *
     * @param object the object for comparing.
     * @return boolean value
     */
    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object == this) {
            return true;
        }

        if (object instanceof Author) {
            Author other = (Author) object;
            if (((Author) object).getClass().getSimpleName().equals(this.getClass().getSimpleName())
                    & (other.firstName.toLowerCase().equals(this.firstName.toLowerCase()))
                    & (other.lastName.toLowerCase().equals(this.lastName.toLowerCase()))
                //& (other.birthDate.equals(this.birthDate))
                    ) {
                return true;
            }
        }
        return false;
    }
}
