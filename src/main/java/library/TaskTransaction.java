package library;

import library.task.Task;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class TaskTransaction {
    private static Socket socket;
    private  static BufferedOutputStream bos ;
    private  static ObjectOutputStream oos ;
    private  static BufferedInputStream bis ;
    private  static ObjectInputStream ois ;


    public static Object run(String className, String methodName) throws IOException, ClassNotFoundException {
        Object objectToReturn ;
        initSocket();
        bos = new BufferedOutputStream(getSocket().getOutputStream());
        oos=new ObjectOutputStream(bos);

        Task task = new Task(className, methodName);
        oos.writeObject(task);
        oos.flush();

        bis=new BufferedInputStream(getSocket().getInputStream());
        ois=new ObjectInputStream(bis);

        objectToReturn = ois.readObject();

        oos.close();
        bos.close();
        ois.close();
        bis.close();

        socket.close();

        return objectToReturn;
    }

    public static Object run(String className, String methodName, Object params) throws IOException, ClassNotFoundException {
        Object objectToReturn ;
        initSocket();
        bos=new BufferedOutputStream(getSocket().getOutputStream());
        oos=new ObjectOutputStream(bos);

        Task task = new Task(className, methodName, params);
        oos.writeObject(task);
        oos.flush();

        bis=new BufferedInputStream(getSocket().getInputStream());
        ois=new ObjectInputStream(bis);

        objectToReturn = ois.readObject();

        oos.close();
        bos.close();
        ois .close();
        bis.close();

        socket.close();

        return objectToReturn;
    }

    public static Object run(String methodName, Object params) throws IOException, ClassNotFoundException {
        Object objectToReturn ;
        initSocket();
        bos=new BufferedOutputStream(getSocket().getOutputStream());
        oos=new ObjectOutputStream(bos);

        Task task = new Task(methodName, params);
        oos.writeObject(task);
        oos.flush();

        bis=new BufferedInputStream(getSocket().getInputStream());
        ois=new ObjectInputStream(bis);

        objectToReturn = ois.readObject();

        oos.close();
        bos.close();
        ois.close();
        bis.close();

        socket.close();

        return objectToReturn;
    }


    public static Socket getSocket() {
        return socket;
    }

    public static void setSocket(Socket socket) {
        TaskTransaction.socket = socket;
    }

    public static void initSocket() {
        try {
            socket = new Socket(InetAddress.getByName("127.0.0.1"), 8081);
        } catch (Exception e) {}
    }


}
