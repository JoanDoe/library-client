package library;

/// {RunByHand}

import library.model.Author;
import library.task.Task;

import java.net.*;

import java.io.*;
import java.util.List;

public class Client {
	public static void main(String[] args) throws IOException {

		InetAddress addr = InetAddress.getByName("127.0.0.1");
		System.out.println("addr = " + addr);
		Socket socket = new Socket(addr, 8081);
		// �������� ��� � ���� try-finally, �����
		// ���� ���������, ��� ����� ���������:
		try {
			BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
			ObjectOutputStream oos = new ObjectOutputStream(bos);


			Task obj = new Task("Author", "getAll", "");
			obj.setParam(130);
			oos.writeObject(obj);
			oos.flush();

			BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
			ObjectInputStream ois = new ObjectInputStream(bis);

			List<Author> authors=null;
			try {
				authors=(List<Author>) ois.readObject();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			//System.out.println("value: " + authors.);
			System.out.println("socket = " + socket);
			// ����� ������������� Output ������������� PrintWriter'��.
			oos.close();
			ois.close();
		} finally {

			System.out.println("closing...");
			socket.close();
		}
	}
}
