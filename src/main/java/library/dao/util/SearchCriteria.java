package library.dao.util;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

public class SearchCriteria implements Serializable {
	private HashMap<String, String> text = new HashMap<String, String>();

	private HashMap<String, Date> dateFrom = new HashMap<String, Date>();

	private HashMap<String, Date> dateTo = new HashMap<String, Date>();

	private String isGiven;

	public HashMap<String, String> getText() {
		return text;
	}

	public void setText(HashMap<String, String> text) {
		this.text = text;
	}

	public HashMap<String, Date> getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(HashMap<String, Date> dateFrom) {
		this.dateFrom = dateFrom;
	}

	public HashMap<String, Date> getDateTo() {
		return dateTo;
	}

	public void setDateTo(HashMap<String, Date> dateTo) {
		this.dateTo = dateTo;
	}

	public String getIsGiven() {
		return isGiven;
	}

	public void setIsGiven(String isGiven) {
		this.isGiven = isGiven;
	}
}
